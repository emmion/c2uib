/*
 * Copyright 2010 - 2012 Creative Consultants LLC.  All rights reserved.
 *
 * Creative Consultants and its licenser retain all intellectual property
 * and proprietary rights in and to this software and related documentation.
 * Any use, reproduction, disclosure, or distribution of this software
 * and related documentation without an express license agreement from
 * Creative Consultants is strictly prohibited.
 *
 */
#pragma once

#include <stddef.h>
#include <stdint.h>

#include <vector>

#include "rdma_link.h"

namespace c2uib
{

#ifndef RDMAPORT
#define RDMAPORT 7174
#endif

class RDMA_Cluster;

int c4uib_seed_process(RDMA_Cluster** cluster, int argc, char** argv);

typedef uint64_t ELEMENT_ID;
const ELEMENT_ID INVALID_ID = (uint64_t)-1;   // invalid cluster element id
//Helper class, container of RDMA links, registered local memory, and shared remote memory'
class RDMA_Cluster
{
    void connectNodes(std::vector<RDMA_Link>& links, uint32_t hca_index, char** list, int port=RDMAPORT);
    
    //RDMA_Link* first_;   // array of linked channels to all HCA nodes (self is not initialized)
    uint32_t node_index_;// HCA node index in this cluster
    uint32_t node_count_;

    std::vector<RDMA_Link> rdma_link_vector_;
    
public:
    RDMA_Cluster(uint32_t hca_index, uint32_t count, char** hca_names);
    ~RDMA_Cluster();
    bool isCluster()
    {
        return(rdma_link_vector_.size() > 0);
    }

    uint32_t getHCAIndex(void)
    {
        return (node_index_);
    }

    uint32_t getHCACount(void)
    {
        return (node_count_);
    }

    void registerMemory(void* addr, size_t size, uint32_t share_id= (uint32_t)-1);
    void rdmaWrite(uint32_t local_reg, size_t local_offset, uint32_t remote_node, uint32_t remote_reg, size_t remote_offset, size_t size, bool block=false);
    void initSync(uint32_t link);
    void procSync(uint32_t link);
    void initSync(uint32_t link, void* msg_in, size_t size);
    void procSync(uint32_t link, void* msg_out, size_t size);
};

} // namespace c2uib

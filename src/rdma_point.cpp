/*
 * Copyright 2016 Emmion.  All rights reserved.
 * Copyright 2016 Gary Scantlen. All rights reserved.
 * 
 * This file is part of c2uib.
 * 
 * c2uib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * c2uib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with c2uib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "rdma_point.h"

#include <errno.h>
#include <stdio.h>

namespace c2uib
{

/////////  RDMA_Point  ////////////////


const int CONNECT_TIMEOUT=10;

void RDMA_Point::connect(char* host, int port)
{
    link=new RDMA_Link();

    if (!link)
    {
        fputs("WTH!\n",stderr);
        return;//TODO: Throw exception
    }

    link->SetPortNum(port);

    if (!host) //start server
    {
        //fputs("server!\n",stderr);
        if (!link->Listen(CONNECT_TIMEOUT))
        {
            fputs("Connect Failed\n",stderr);
        };
    }
    else //start client
    {
        //fputs("client!\n",stderr);
        if (!link->Connect(host,CONNECT_TIMEOUT))
        {
            fputs("Connect Failed\n",stderr);
        };
    }
}

void RDMA_Point::recvReg(void)
{
    link->recv_shared_info();
    link->recv_wait();
}

void RDMA_Point::regMem(void* addr, size_t size, uint32_t share_id)
{
    // register and share this memory on link
    link->register_memory(addr, size, share_id!=(uint32_t)-1?true:false, share_id);
}

bool RDMA_Point::write(size_t size, bool block)
{
    if (!block)
    {
        return (link->shmem_write(link->GetMemoryRegion(0), 0, &(link->GetRemoteSharedMemoryInfo(0)), 0, size));
    }

    RDMA_Link::WORK_ID wr_id;

    if (!link->shmem_write(link->GetMemoryRegion(0), 0, &(link->GetRemoteSharedMemoryInfo(0)), 0, size, (uint64_t)&wr_id))
    {
        return (false);
    }

    wr_id.complete();
    return (true);
}
bool RDMA_Point::read(size_t size, bool block)
{
    if (!block)
    {
        return (link->shmem_read(&(link->GetRemoteSharedMemoryInfo(0)), 0, link->GetMemoryRegion(0), 0, size));
    }

    RDMA_Link::WORK_ID wr_id;

    if (!link->shmem_read(&(link->GetRemoteSharedMemoryInfo(0)), 0, link->GetMemoryRegion(0), 0, size, (uint64_t)&wr_id))
    {
        return (false);
    }

    wr_id.complete();
    return (true);
}
void RDMA_Point::initSync(void)
{
    link->initSync();//message_recv(sync_msg,sizeof(sync_msg));
}

void RDMA_Point::procSync(void)
{
    //link->message_send( sync_msg, sizeof(sync_msg), true);
    link->procSync();//recv_wait();
}

RDMA_Point::RDMA_Point(char* host, int port)
{
    connect(host,port);
}
RDMA_Point::RDMA_Point(char* host)
{
    connect(host);
}
RDMA_Point::RDMA_Point()
{
    connect();
}
RDMA_Point::~RDMA_Point()
{
    if (link)
    {
        delete link;
    }
}


} // namespace c2uib

/*
 * Copyright 2010 - 2012 Creative Consultants LLC.  All rights reserved.
 *
 * Creative Consultants and its licenser retain all intellectual property
 * and proprietary rights in and to this software and related documentation.
 * Any use, reproduction, disclosure, or distribution of this software
 * and related documentation without an express license agreement from
 * Creative Consultants is strictly prohibited.
 *
 */

#pragma once

#include <rdma/rdma_cma.h>
#include <semaphore.h>

#include <vector>//TODO: replace with custom container

namespace c2uib
{

typedef struct
{
    uint32_t id;//cluster wide unique identifier, ie process id = (proc_index * num_nodes + node_index)
    void* addr;
    size_t length;
    uint32_t key;
}	SharedMemoryInfo;

typedef std::vector<SharedMemoryInfo> SharedMemoryInfoVector;
typedef std::vector<struct ibv_mr*> MemoryRegionVector;


#define SAMPLE_FREQ 10 //sample freq Hz for timeout
#define LISTEN_TIMEOUT 3 //does it do any good ?

enum
{
    NOT_CONNECTED,
    SERVER_CONNECTING,
    SERVER_CONNECTED,
    CLIENT_CONNECTING,
    CLIENT_CONNECTED,
    CONNECT_FAILED
};

/**
 * @brief ...
 * 
 */
class RDMA_Link
{
public:

    class WORK_ID
    {
    public:
        WORK_ID();
        ~WORK_ID();
        void add_wr();
        void rm_wr();
        void complete();
        pthread_mutex_t mutex;
        pthread_cond_t cond;
        int count;
    };


    /**
     * @brief Constructor
     *
     */
    RDMA_Link(void);
    
    /**
     * @brief Destructor
     * 
     */
    ~RDMA_Link(void);
    
    int Connect(char* to_srvr, int timeout=0);
    int Listen(int timeout=0);
    static void* server_thread(void* arg);
    static void* cq_thread(void* arg);
    int complete(void);
    int server_complete(void);
    int client_complete(void);
    int block_till_send_wrq_available(struct ibv_send_wr** wr, struct ibv_sge** sge);
    int block_till_recv_wrq_available(struct ibv_recv_wr** wr, struct ibv_sge** sge);

    void recv_shared_info(uint32_t count=1);
    struct ibv_mr* register_memory(void* addr, size_t size, bool share=false, uint32_t share_id=0);
//	int memory_copy(struct ibv_mr *l_memory, size_t l_index,  SHARED_MEM_INFO *r_memory, size_t r_index, size_t size, bool wimm=false, uint32_t imm_data=0);
//	int memory_copy( SHARED_MEM_INFO *r_memory, size_t r_index, struct ibv_mr *l_memory, size_t l_index, size_t size );//, bool wimm=false, uint32_t imm_data=0);
//	int memory_write(void *l_addr, uint32_t l_key, void *r_addr, uint32_t r_key, size_t size, bool wimm=false, uint32_t imm_data=0);
//	int memory_read(void *l_addr, uint32_t l_key, void *r_addr, uint32_t r_key, size_t size);
    int shmem_write(struct ibv_mr* l_memory, size_t l_index,  SharedMemoryInfo* r_memory, size_t r_index, size_t size, uint64_t wr_id=0);//, bool wait_return=false );
    int shmem_read(SharedMemoryInfo* r_memory, size_t r_index, struct ibv_mr* l_memory, size_t l_index, size_t size, uint64_t wr_id=0); //, bool wait_return=false );
//	void write_done(uint64_t wr_id=0);
//	void write_wait(uint64_t wr_id=0);
    void read_done(uint64_t wr_id=0);
    void read_wait(uint64_t wr_id=0);

    int message_recv(void* msg, size_t size);
    int message_send(void* msg, size_t size, bool wait_return=false);
    int imm_data_recv(uint64_t id=0);

    inline void recv_wait(void)
    {
        sem_wait(&wait_recv_sem);
    }
//	inline void send_wait(void){sem_wait(&wait_send_msg);};
//	inline void read_wait(void){sem_wait(&wait_read_sem);};
//	inline void write_wait(void){sem_wait(&wait_write_sem);};
    inline void wimm_wait(void)
    {
        sem_wait(&wait_wimm_sem);
    }

    /**
     * @brief ...
     * 
     * @param port_num p_port_num:...
     */
    void SetPortNum(uint16_t port_num)
    {
      port_number_ = port_num;
    }
    
    /**
     * @brief ...
     * 
     * @return uint16_t
     */
    uint16_t GetPortNum() const
    {
      return(port_number_);
    }

    /**
     * @brief ...
     * 
     * @return c2uib::MemoryRegionVector&
     */
    MemoryRegionVector& GetMemoryRegionVector()
    {
      return(memory_region_vector_);
    }
    
    /**
     * @brief Returns a pointer to the memory region at the given index
     * 
     * @param index memory region iindex
     * @return struct ibv_mr* pointer to memory region
     */
    struct ibv_mr* GetMemoryRegion(size_t index)
    {
      return(GetMemoryRegionVector().at(index));
    }
    
    SharedMemoryInfo& GetLocalSharedMemoryInfo(size_t index)
    {
      return(local_shared_mem_info_vector_.at(index));
    }
    
    SharedMemoryInfo& GetRemoteSharedMemoryInfo(size_t index)
    {
      return(remote_shared_mem_info_vector_.at(index));
    }

    /**
     * @brief ...
     * 
     */
    void initSync(void);
    
    /**
     * @brief ...
     * 
     */
    void procSync(void);

private:
  
    int connect_state;
    struct sockaddr_storage sockaddr;
    
    /* port in network bite order */
    uint16_t port_number_;
    
    pthread_t srvrthrdid, cqthrdid;

    struct rdma_cm_id* cm_id;
    struct rdma_cm_id* child_cm_id;
    struct rdma_event_channel* cm_channel;
    struct rdma_conn_param conn_param;

    struct ibv_comp_channel* channel;
    struct ibv_cq* cq;
    struct ibv_pd* pd;
    struct ibv_qp* qp;

    struct ibv_send_wr* send_wr_list;
    struct ibv_sge* send_sge_list;
    struct ibv_recv_wr* recv_wr_list;
    struct ibv_sge* recv_sge_list;
    sem_t wait_send_wr;
    sem_t wait_recv_wr;

    /** Vector of pointers to ibv_mr regions */
    MemoryRegionVector memory_region_vector_;

    /** Local shared memory info */
    SharedMemoryInfoVector local_shared_mem_info_vector_;

    /** Remote shared memory info */
    SharedMemoryInfoVector remote_shared_mem_info_vector_;

    struct ibv_mr* recv_msg_mr;
    struct ibv_mr* send_msg_mr;
    sem_t wait_recv_sem;
    sem_t wait_send_msg;
    //sem_t wait_read_sem;
    //sem_t wait_write_sem;
    sem_t wait_wimm_sem;

    char sync_msg[64];

};

} // namespace c2uib

/*
 * Copyright 2010 - 2012 Creative Consultants LLC.  All rights reserved.
 *
 * Creative Consultants and its licenser retain all intellectual property
 * and proprietary rights in and to this software and related documentation.
 * Any use, reproduction, disclosure, or distribution of this software
 * and related documentation without an express license agreement from
 * Creative Consultants is strictly prohibited.
 *
 */



#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <netdb.h>

#include "rdma_link.h"
//#include "rdma_cluster.h"

namespace c2uib
{

//TODO: add cq & qp.pthread_mutex_t to read/write protected memory;
#define MAX_SEND_WR 16 //16
#define MAX_RECV_WR 2 //2
#define MIN_CQ_ENTRIES 20
#define MAX_SEND_SGE 1
#define MAX_RECV_SGE 1






//////////////////////////////////////////////////////////////////////////////////////////






//////////////////////////////////////////////////////////////////////////////////////////



void RDMA_Link::initSync(void)
{
    message_recv(sync_msg,sizeof(sync_msg));
}

void RDMA_Link::procSync(void)
{
    message_send(sync_msg, sizeof(sync_msg), true);
    recv_wait();
}



RDMA_Link::RDMA_Link(void)
: port_number_(0)
{
//	index = count++;
    connect_state = NOT_CONNECTED;
    memset(&sockaddr, 0, sizeof(struct sockaddr_storage));
    sockaddr.ss_family = PF_INET;//PF_INET6;

    child_cm_id = cm_id = 0;

    sem_init(&wait_recv_sem, 0, 0);
    sem_init(&wait_send_msg, 0, 0);
    //sem_init(&wait_read_sem, 0, 0);
    //sem_init(&wait_write_sem, 0, 0);
    sem_init(&wait_wimm_sem, 0, 0);

    send_wr_list=new ibv_send_wr[MAX_SEND_WR];
    send_sge_list=new ibv_sge[MAX_SEND_WR];
    sem_init(&wait_send_wr, 0, MAX_SEND_WR);
    recv_wr_list=new ibv_recv_wr[MAX_RECV_WR];
    recv_sge_list=new ibv_sge[MAX_RECV_WR];
    sem_init(&wait_recv_wr, 0, MAX_RECV_WR);

//	pthread_mutex_init(&count_read_mutex,0);
//	pthread_mutex_init(&count_write_mutex,0);
//	pthread_cond_init(&count_read_cond, 0);
//	pthread_cond_init(&count_write_cond, 0);
//	count_read=0;
//	count_write=0;

    if (!(cm_channel = rdma_create_event_channel()))
    {
        fputs("rdma_create_event_channel error\n",stderr);
        return;
    }

    if (rdma_create_id(cm_channel, &cm_id, this, RDMA_PS_TCP))
    {
        fputs("rdma_create_id error\n",stderr);
        rdma_destroy_event_channel(cm_channel);
        return;
    }
}



RDMA_Link::~RDMA_Link(void)
{
    if (cm_id != child_cm_id
            && rdma_destroy_id(cm_id))
    {
        fputs("rdma_destroy_id failed\n",stderr);
    }

    rdma_destroy_event_channel(cm_channel);

    delete[] send_wr_list;
    delete[] send_sge_list;
}



// Connect server timeout in seconds (non-blocking if timeout=0)
int RDMA_Link::Listen(int timeout)
{
    if (connect_state == NOT_CONNECTED)
    {
        connect_state = SERVER_CONNECTING;
        pthread_create(&srvrthrdid, NULL, server_thread, (void*)this);
    }

    //server wait timeout for client's connect request
    timeout*=SAMPLE_FREQ;
    int i=0;

    if (connect_state==SERVER_CONNECTING)
    {
        while (connect_state==SERVER_CONNECTING)
        {
            if (i++<timeout)
            {
                usleep(1000000/SAMPLE_FREQ);
            }
            else
            {
                return 0;
            }
        }

        void* vp=0;
        pthread_join(srvrthrdid, &vp);

        if (vp!=this)
        {
            return 0;
        }
    }

    return 1;
}



void* RDMA_Link::server_thread(void* arg)
{
    RDMA_Link* contxt=(RDMA_Link*)arg;

    if (contxt->sockaddr.ss_family == AF_INET)
    {
        ((struct sockaddr_in*) &contxt->sockaddr)->sin_port = htons(contxt->port_number_);
    }
    else
    {
        ((struct sockaddr_in6*) &contxt->sockaddr)->sin6_port = htons(contxt->port_number_);
    }

    if (rdma_bind_addr(contxt->cm_id, (struct sockaddr*) &contxt->sockaddr)!=0)
    {
        fputs("rdma_bind_addr error\n",stderr);
        pthread_exit((void*)(contxt->connect_state=CONNECT_FAILED));
    }

    if (rdma_listen(contxt->cm_id, LISTEN_TIMEOUT)!=0)
    {
        fputs("rdma_listen failed: %d\n",stderr);
        pthread_exit((void*)(contxt->connect_state=CONNECT_FAILED));
    }

    struct rdma_cm_event* event=0;

    if (rdma_get_cm_event(contxt->cm_channel, &event)!=0
            || event==0
            || event->event != RDMA_CM_EVENT_CONNECT_REQUEST)
    {
        fprintf(stderr, "%s, not conn req.\n",rdma_event_str(event->event));
        pthread_exit((void*)(contxt->connect_state=CONNECT_FAILED));
    }

    contxt->child_cm_id = event->id;

    if (rdma_ack_cm_event(event))
    {
        fputs("rdma_ack_cm_event error\n",stderr);
        pthread_exit((void*)(contxt->connect_state=CONNECT_FAILED));
    }

    //good, null connect_state will flag connect
    if (!contxt->server_complete())
    {
        pthread_exit((void*)(contxt->connect_state=CONNECT_FAILED));
    }

    contxt->connect_state=SERVER_CONNECTED;
    pthread_exit(arg);
}



int RDMA_Link::server_complete(void)
{
    //complete connection
    if (!complete())
    {
        return (0);
    }

    struct rdma_cm_event* event=0;

    //server accepts the connect request
    if (rdma_accept(child_cm_id, &conn_param))
    {
        fputs("rdma_accept error\n",stderr);
        return 0;
    }

    if (rdma_get_cm_event(cm_channel, &event)!=0
            || event==0
            || event->event != RDMA_CM_EVENT_ESTABLISHED)
    {
        fputs(rdma_event_str(event->event),stderr);
        fputs(" did not finish connect w/ established\n",stderr);
        return 0;
    }

    rdma_ack_cm_event(event);

    // start the completion queue service thread
    pthread_create(&cqthrdid, NULL, cq_thread, this);

    return 1;
}


// Connect client
int RDMA_Link::Connect(char* server, int timeout)
{
    //resolves RDMA_CM_EVENT_ADDR & ROUTE
    connect_state=CLIENT_CONNECTING;
    struct addrinfo* res;

    if (getaddrinfo(server, NULL, NULL, &res) || res==0)
    {
        printf("getaddrinfo failed - invalid hostname or IP address\n");
        return 0;
    }

    if (res->ai_family == PF_INET)
    {
        memcpy(&sockaddr, res->ai_addr, sizeof(struct sockaddr_in));
        ((struct sockaddr_in*) &sockaddr)->sin_port = htons(port_number_);
    }
    else //(res->ai_family == PF_INET6)
    {
        memcpy(&sockaddr, res->ai_addr, sizeof(struct sockaddr_in6));
        ((struct sockaddr_in6*) &sockaddr)->sin6_port = htons(port_number_);
    }

    freeaddrinfo(res);

    timeout*=SAMPLE_FREQ;
    int retry=-1,i=0;//timeout, i counts usleeps in complete()

    while (retry==-1) //while connect time out
    {
        if (rdma_resolve_addr(cm_id, NULL, (struct sockaddr*)&sockaddr, 1000)) //1.000 sec timeout
        {
            perror("rdma_resolve_addr");
            return 0;
        }

        struct rdma_cm_event* event=0;

        if (rdma_get_cm_event(cm_channel, &event)!=0
                || event==0
                || event->event != RDMA_CM_EVENT_ADDR_RESOLVED)
        {
            fprintf(stderr, "%s, bad address.\n",rdma_event_str(event->event));
            return 0;
        }

        if (rdma_ack_cm_event(event))
        {
            fputs("rdma_ack_cm_event error\n",stderr);
            return 0;
        }

        event=0;

        if (rdma_resolve_route(cm_id, 1000))//1.000 sec timeout
        {
            fputs("rdma_resolve_route error\n",stderr);
            return 0;
        }

        if (rdma_get_cm_event(cm_channel, &event)!=0
                || event==0
                || event->event != RDMA_CM_EVENT_ROUTE_RESOLVED)
        {
            fprintf(stderr, "%s, bad address.\n",rdma_event_str(event->event));
            return 0;
        }

        child_cm_id = event->id;

        rdma_ack_cm_event(event);

        if (i++<timeout)
        {
            retry=client_complete();    //-1=retry
        }
        else
        {
            retry=0;
        }
    }

    return retry;//return complete();
}



int RDMA_Link::client_complete(void)
{
    //complete connection
    if (!complete())
    {
        return (0);
    }

    struct rdma_cm_event* event=0;

    //client sends connect request and waits for server's accept
    if (rdma_connect(child_cm_id, &conn_param))
    {
        fputs("rdma_connect error\n",stderr);
        return 0;
    }

    if (rdma_get_cm_event(cm_channel, &event)!=0 || !event)
    {
        fputs("rdma_get_cm_event error\n",stderr);
        return 0;
    }

    if (event->event == RDMA_CM_EVENT_REJECTED)
    {
        rdma_ack_cm_event(event);
        usleep(1000000/SAMPLE_FREQ);
        fputc('*',stderr);
        rdma_destroy_qp(child_cm_id);
        rdma_destroy_id(cm_id);//(child_cm_id);

        if (rdma_create_id(cm_channel, &cm_id, this, RDMA_PS_TCP))
        {
            fputs("rdma_create_id error\n",stderr);
            return 0;
        }

        return -1;//-1=retry
    }

    if (event->event != RDMA_CM_EVENT_ESTABLISHED)
    {
        fputs(rdma_event_str(event->event),stderr);
        fputs(" did not finish connect w/ established\n",stderr);
        return 0;
    }

    rdma_ack_cm_event(event);

    // start the completion queue service thread
    pthread_create(&cqthrdid, NULL, cq_thread, this);

    return 1;
}



int RDMA_Link::complete(void)
{
    //allocate and create completion queue
    if (!(pd=ibv_alloc_pd(child_cm_id->verbs)))
    {
        fputs("ibv_alloc_pd error\n",stderr);
        return 0;
    }

    if (!(channel = ibv_create_comp_channel(child_cm_id->verbs)))
    {
        fputs("ibv_create_comp_channel error\n",stderr);
        return 0;
    }

    if (!(cq = ibv_create_cq(child_cm_id->verbs, MIN_CQ_ENTRIES, this, channel, 0)))
    {
        fputs("ibv_create_cq error\n",stderr);
        return 0;
    }

    //initialize and create queue pair
    struct ibv_qp_init_attr init_attr;
    memset(&init_attr, 0, sizeof(init_attr));
    init_attr.cap.max_send_wr = MAX_SEND_WR;
    init_attr.cap.max_recv_wr = MAX_RECV_WR;
    init_attr.cap.max_recv_sge = MAX_RECV_SGE;
    init_attr.cap.max_send_sge = MAX_SEND_SGE;
    init_attr.qp_type = IBV_QPT_RC;
    init_attr.send_cq = cq;
    init_attr.recv_cq = cq;

    if (rdma_create_qp(child_cm_id, pd, &init_attr))
    {
        fputs("create_qp failed\n",stderr);
        return 0;
    }

    qp = child_cm_id->qp;

    //initialize connection
    memset(&conn_param, 0, sizeof(struct rdma_conn_param));
    conn_param.responder_resources = 1;
    conn_param.initiator_depth = 1;
    //conn_param.retry_count = 10;
    conn_param.rnr_retry_count = 7;//10;

    return (1);
}



int RDMA_Link::block_till_recv_wrq_available(struct ibv_recv_wr** wr, struct ibv_sge** sge)
{
    int wr_index;
    sem_wait(&wait_recv_wr);
    sem_getvalue(&wait_recv_wr,&wr_index);//FIXME: is this safe, we need custom semaphore ???

    if (wr_index>=MAX_RECV_WR || wr_index<0)
    {
        fputs("WTH****MAX_RECV_WR*****\n",stderr);
    }

    *wr=recv_wr_list+wr_index;
    *sge=recv_sge_list+wr_index;
    memset(*wr, 0, sizeof(struct ibv_recv_wr));//FIXME: should be filled by predefined values for a specified communications type
    return wr_index;
}



int RDMA_Link::block_till_send_wrq_available(struct ibv_send_wr** wr, struct ibv_sge** sge)
{
    int wr_index;
    sem_wait(&wait_send_wr);
    sem_getvalue(&wait_send_wr,&wr_index);//FIXME: is this safe, we need custom semaphore ???

    if (wr_index>=MAX_SEND_WR || wr_index<0)
    {
        fputs("WTH****MAX_SEND_WR*****\n",stderr);
    }

    *wr=send_wr_list+wr_index;
    *sge=send_sge_list+wr_index;
    memset(*wr, 0, sizeof(struct ibv_send_wr));//FIXME: should be filled by predefined values for a specified communications type
    return wr_index;
}



void* RDMA_Link::cq_thread(void* arg)
{
    RDMA_Link* contxt=(RDMA_Link*)arg;

    //start first_ notification
    if (ibv_req_notify_cq(contxt->cq, 0))
    {
        fputs("ibv_req_notify_cq failed\n",stderr);
        pthread_exit(NULL);
    }

    //struct ibv_recv_wr *bad_wr=0, imm_wr={0,0,0,0};
    struct ibv_cq* ev_cq;
    struct ibv_wc wc;

    while (1)
    {
        pthread_testcancel();

        if (!ibv_get_cq_event(contxt->channel, &ev_cq, &arg)
                //&& (arg==(void*)contxt)
                //&& (ev_cq==contxt->cq)
                && !ibv_req_notify_cq(contxt->cq, 0))
        {
            while (ibv_poll_cq(contxt->cq, 1, &wc)==1)//ask for 1 queue get 1 queue
                if (wc.status == IBV_WC_SUCCESS)//IBV_WC_WR_FLUSH_ERR)
                {
                    if (wc.opcode & IBV_WC_RECV)
                    {
                        sem_post(&(contxt->wait_recv_wr));
                    }
                    else
                    {
                        sem_post(&(contxt->wait_send_wr));
                    }

                    switch (wc.opcode)
                    {
                    case IBV_WC_RDMA_WRITE:

                        //contxt->write_done(wc.wr_id);
                        if (wc.wr_id) //contxt->rm_wr_from_id((WORK_ID*)wc.wr_id);
                        {
                            ((WORK_ID*)wc.wr_id)->rm_wr();
                        }

                        //sem_trywait(&(contxt->wait_write_sem));
                        //sem_post(&(contxt->wait_write_sem));//set recv semaphore
                        //fputs("IBV_WC_RDMA_WRITE\n",stderr);
                        break;

                    case IBV_WC_RDMA_READ:

                        //contxt->read_done(wc.wr_id);
                        if (wc.wr_id) //contxt->rm_wr_from_id((WORK_ID*)wc.wr_id);
                        {
                            ((WORK_ID*)wc.wr_id)->rm_wr();
                        }

                        //sem_trywait(&(contxt->wait_read_sem));
                        //sem_post(&(contxt->wait_read_sem));//set recv semaphore
                        //fputs("IBV_WC_RDMA_READ\n",stderr);
                        break;

                    case IBV_WC_RECV:
                        sem_post(&(contxt->wait_recv_sem));//set recv semaphore
                        //fputs("IBV_WC_RECV\n",stderr);
                        //ibv_post_recv(contxt->qp, &imm_wr, &bad_wr);
                        break;

                    case IBV_WC_SEND:
                        sem_post(&(contxt->wait_send_msg));//set recv semaphore
                        //fputs("IBV_WC_SEND\n",stderr);
                        break;

                    case IBV_WC_RECV_RDMA_WITH_IMM:
                        sem_post(&(contxt->wait_wimm_sem));//set recv semaphore
                        //fputs("IBV_WC_RECV_RDMA_WITH_IMM\n",stderr);
                        break;

                    default:
                        fprintf(stderr,"unknown CQ code %d\n",wc.opcode);
                        break;
                    }
                }
                else
                {
                    fprintf(stderr,"bad WC.STATUS = %d\n",wc.status);//fputs("IBV_WC_WR_FLUSH_ERR\n",stderr);
                    break;
                }
        }
        else if (errno == EINTR)
        {
            fputs("WTH: EINTR\n",stderr);
        }
        else
        {
            break;
        }

        ibv_ack_cq_events(ev_cq, 1);//And ack only 1 queue
    }

    fprintf(stderr,"CQ event failed!: %s \n",strerror(errno));

    pthread_exit(NULL);
}






int RDMA_Link::message_recv(void* msg, size_t size)
{
    while (!sem_trywait(&wait_recv_sem)); //clear recv semaphore

    //setup queue to receive remote sides connect status
    if (!(recv_msg_mr=ibv_reg_mr(pd, msg, size, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE)))
    {
        fputs("recv_buf reg_mr failed\n",stderr);
        return 0;
    }

//	struct ibv_recv_wr wr, *bad_wr = NULL;
//	struct ibv_sge sge;
//
//	memset(&wr, 0, sizeof(wr));
//	wr.wr_id = NULL;//(uintptr_t)conn;
//	wr.next = NULL;
//	wr.sg_list = &sge;
//	wr.num_sge = 1;
//
//	sge.addr = (uintptr_t)msg;
//	sge.length = size;
//	sge.lkey = recv_msg_mr->lkey;

    struct ibv_recv_wr* wr;
    struct ibv_sge* sge;
    block_till_recv_wrq_available(&wr,&sge);
    wr->wr_id = NULL;//(uintptr_t)conn;
    wr->next = NULL;
    wr->sg_list = sge;
    wr->num_sge = 1;
    sge->addr = (uintptr_t)msg;
    sge->length = size;
    sge->lkey = recv_msg_mr->lkey;

    struct ibv_recv_wr* bad_wr = NULL;

    if (ibv_post_recv(qp, wr, &bad_wr))
    {
        fputs("ibv_post_recv error\n",stderr);
        return 0;
    }

    return (1);
}



int RDMA_Link::imm_data_recv(uint64_t)// id)
{
    while (!sem_trywait(&wait_wimm_sem)); //clear read semaphore

//	struct ibv_recv_wr wr={id,0,0,0}, *bad_wr = NULL;
    struct ibv_recv_wr* wr;
    struct ibv_sge* sge;
    block_till_recv_wrq_available(&wr,&sge);

    struct ibv_recv_wr* bad_wr = NULL;

    if (ibv_post_recv(qp, wr, &bad_wr))
    {
        fputs("ibv_post_recv error\n",stderr);
        return 0;
    }

    return (1);
}



struct ibv_mr* RDMA_Link::register_memory(void* addr, size_t size, bool share, uint32_t share_id)
{
    memory_region_vector_.push_back(
        ibv_reg_mr(pd, addr, size, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_REMOTE_READ));

    if (share)
    {
        local_shared_mem_info_vector_.push_back((SharedMemoryInfo){share_id, addr, size, memory_region_vector_.back()->rkey});
        message_send(&local_shared_mem_info_vector_.back(), sizeof(SharedMemoryInfo), true);
    }

    return (memory_region_vector_.back());
}



void RDMA_Link::recv_shared_info(uint32_t count) //TODO: add multiple semi_wait handler
{
    for (uint32_t i=0; i<count; i++) //TODO: implement sge with count > 1
    {
        remote_shared_mem_info_vector_.push_back((SharedMemoryInfo){0,0,0,0});
        message_recv(&(remote_shared_mem_info_vector_.back()), sizeof(SharedMemoryInfo));
    }
}

//void RDMA_Link::initiate_fence(void)
//{
//	message_recv(fence_data_,sizeof(fence_data_));
//}
//
//void RDMA_Link::apply_fence(void)
//{
//	message_send( fence_data_, sizeof(fence_data_), true);
//	recv_wait();
//}


int RDMA_Link::message_send(void* msg, size_t size, bool wait_return)
{
    if (wait_return) while (!sem_trywait(&wait_send_msg)); //clear send semaphore

    //setup queue to send connect status to remote side
    if (!(send_msg_mr=ibv_reg_mr(pd, msg, size, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE)))
    {
        fputs("send_buf reg_mr failed\n",stderr);
        return 0;
    }

////	fprintf(stderr,"%d) %d\n",index,temp_count++);
//	sem_wait(&wait_send_wr);
//
//	struct ibv_send_wr wr, *bad_wr = NULL;
//	struct ibv_sge sge;
//
//	memset(&wr, 0, sizeof(wr));
//	wr.wr_id = NULL;//(uintptr_t)conn;
//	wr.opcode = IBV_WR_SEND;
//	wr.sg_list = &sge;
//	wr.num_sge = 1;
//	wr.send_flags = IBV_SEND_SIGNALED;
//
//	sge.addr = (uintptr_t)msg;
//	sge.length = size;
//	sge.lkey = send_msg_mr->lkey;
//
//	if (ibv_post_send(qp, &wr, &bad_wr))
//	{	fputs("ibv_post_send error\n",stderr);
//		return 0;
//	}

//	int wr_index;
//	sem_wait(&wait_send_wr);
//	sem_getvalue(&wait_send_wr,&wr_index);
//if(wr_index>=MAX_SEND_WR || wr_index<0)fputs("WTH*********\n",stderr);
//	struct ibv_send_wr *wr=send_wr_list+wr_index;
//	struct ibv_sge *sge=send_sge_list+wr_index;
//	memset(wr, 0, sizeof(struct ibv_send_wr));
    struct ibv_send_wr* wr;
    struct ibv_sge* sge;
    block_till_send_wrq_available(&wr,&sge);

    wr->wr_id = NULL;//(uintptr_t)conn;
    wr->opcode = IBV_WR_SEND;
    wr->sg_list = sge;
    wr->num_sge = 1;
    wr->send_flags = IBV_SEND_SIGNALED;

    sge->addr = (uintptr_t)msg;
    sge->length = size;
    sge->lkey = send_msg_mr->lkey;

    struct ibv_send_wr* bad_wr = NULL;

    if (ibv_post_send(qp, wr, &bad_wr))
    {
        fputs("ibv_post_send error\n",stderr);
        return 0;
    }

    if (wait_return)
    {
        sem_wait(&wait_send_msg);
    }

    return (1);
}


RDMA_Link::WORK_ID::WORK_ID(void)
{
    pthread_mutex_init(&mutex,0);
    pthread_cond_init(&cond, 0);
    count=0;
}

RDMA_Link::WORK_ID::~WORK_ID(void)
{
    pthread_mutex_destroy(&mutex);
}

void RDMA_Link::WORK_ID::add_wr(void)
{
    pthread_mutex_lock(&mutex);
    count++;
    pthread_mutex_unlock(&mutex);
}

void RDMA_Link::WORK_ID::rm_wr(void)
{
    pthread_mutex_lock(&mutex);

    if (!--count)
    {
        pthread_cond_signal(&cond);
    }

    pthread_mutex_unlock(&mutex);
}

void RDMA_Link::WORK_ID::complete(void)
{
    pthread_mutex_lock(&mutex);

    while (count)
    {
        pthread_cond_wait(&cond, &mutex);
    }

    pthread_mutex_unlock(&mutex);
}


int RDMA_Link::shmem_write(struct ibv_mr* l_memory, size_t l_index,  SharedMemoryInfo* r_memory, size_t r_index, size_t size, uint64_t wr_id)//, bool wait_return )
{
    //if(wait_return) while(!sem_trywait(&wait_write_sem)); //clear write semaphore
    //sem_post(&wait_write_sem);
//	pthread_mutex_lock(&count_write_mutex);count_write++;pthread_mutex_unlock(&count_write_mutex);
    if (wr_id) //add_wr_to_id((WORK_ID*)wr_id);
    {
        ((WORK_ID*)wr_id)->add_wr();
    }

//	int wr_index;
//	sem_wait(&wait_send_wr);
//	sem_getvalue(&wait_send_wr,&wr_index);//FIXME: this is not safe, need custom mutex
//if(wr_index>=MAX_SEND_WR || wr_index<0)fputs("WTH*********\n",stderr);
//	struct ibv_send_wr *wr=send_wr_list+wr_index;
//	struct ibv_sge *sge=send_sge_list+wr_index;

    struct ibv_send_wr* wr;
    struct ibv_sge* sge;
    block_till_send_wrq_available(&wr,&sge);

    memset(wr, 0, sizeof(struct ibv_send_wr));
    wr->wr_id = wr_id;
    wr->next = NULL;
    wr->sg_list = sge;
    wr->num_sge = 1;//TODO: implement sge with count > 1
    wr->send_flags = IBV_SEND_SIGNALED;//TODO: does this need to be signaled ????
    wr->opcode = (/*wimm ? IBV_WR_RDMA_WRITE_WITH_IMM : */IBV_WR_RDMA_WRITE);
    //wr->imm_data = 0;//imm_data;
    wr->sg_list->length = size;
    wr->sg_list->addr=(uintptr_t)l_memory->addr + l_index;
    wr->sg_list->lkey = l_memory->lkey;
    wr->wr.rdma.remote_addr = (uintptr_t)r_memory->addr + r_index;
    wr->wr.rdma.rkey = r_memory->key;

    struct ibv_send_wr* bad_wr = NULL;

    if (ibv_post_send(qp, wr, &bad_wr)||bad_wr)
    {
        perror("shmem_write ibv_post_send");
        return (0);
    }

    //if(wait_return) sem_wait(&wait_write_sem);
    return (1);
}


int RDMA_Link::shmem_read(SharedMemoryInfo* r_memory, size_t r_index, struct ibv_mr* l_memory, size_t l_index, size_t size, uint64_t wr_id) //, bool wait_return )
{
    //if(wait_return) while(!sem_trywait(&wait_read_sem)); //clear read semaphore
    //sem_post(&wait_read_sem);
//	pthread_mutex_lock(&count_read_mutex);count_read++;pthread_mutex_unlock(&count_read_mutex);
    if (wr_id) //add_wr_to_id((WORK_ID*)wr_id);
    {
        ((WORK_ID*)wr_id)->add_wr();
    }

//	int wr_index;
//	sem_wait(&wait_send_wr);
//	sem_getvalue(&wait_send_wr,&wr_index);//FIXME: this is not safe, need custom mutex
//if(wr_index>=MAX_SEND_WR || wr_index<0)fputs("WTH*********\n",stderr);
//	struct ibv_send_wr *wr=send_wr_list+wr_index;
//	struct ibv_sge *sge=send_sge_list+wr_index;
    struct ibv_send_wr* wr;
    struct ibv_sge* sge;
    block_till_send_wrq_available(&wr,&sge);

    memset(wr, 0, sizeof(struct ibv_send_wr));
    wr->wr_id = wr_id;
    wr->next = NULL;
    wr->sg_list = sge;
    wr->num_sge = 1;//TODO: implement sge with count > 1
    wr->send_flags = IBV_SEND_SIGNALED;//TODO: does this need to be signaled ????
    wr->opcode = IBV_WR_RDMA_READ;//(wimm ? IBV_WR_RDMA_WRITE_WITH_IMM : IBV_WR_RDMA_WRITE);
    //wr->imm_data = 0;//imm_data;
    wr->sg_list->length = size;
    wr->sg_list->addr=(uintptr_t)l_memory->addr + l_index;
    wr->sg_list->lkey = l_memory->lkey;
    wr->wr.rdma.remote_addr = (uintptr_t)r_memory->addr + r_index;
    wr->wr.rdma.rkey = r_memory->key;

    struct ibv_send_wr* bad_wr = NULL;

    if (ibv_post_send(qp, wr, &bad_wr)||bad_wr)
    {
        perror("shmem_read ibv_post_send");
        return (0);
    }

    //if(wait_return) sem_wait(&wait_read_sem);
    return (1);
}

} // namespace c2uib





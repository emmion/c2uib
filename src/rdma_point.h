
/*
 * Copyright 2016 Emmion.  All rights reserved.
 * Copyright 2016 Gary Scantlen. All rights reserved.
 * 
 * This file is part of c2uib.
 * 
 * c2uib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * c2uib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with c2uib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#pragma once

#include <stdint.h>
#include <inttypes.h>

#include "rdma_link.h"

namespace c2uib
{

#ifndef RDMAPORT
#define RDMAPORT 7174
#endif

/**
 * @brief ...
 * 
 */
class RDMA_Point
{
public:
    void connect(char* host=0, int port=RDMAPORT);
    RDMA_Point();
    RDMA_Point(char* host);
    RDMA_Point(char* host, int port);
    ~RDMA_Point();
    void recvReg(void);
    void regMem(void* addr, size_t size, uint32_t share_id);
    bool write(size_t size, bool block=false);
    bool read(size_t size, bool block=false);
//	char sync_msg[64];
    void initSync(void);
    void procSync(void);

private:
    RDMA_Link* link;

};


  
} // namespace c2uib

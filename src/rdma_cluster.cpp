
/*
 * Copyright 2016 Emmion.  All rights reserved.
 * Copyright 2016 Gary Scantlen.  All rights reserved.
 *
 * This file is part of c2uib.
 *
 * c2uib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * c2uib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with c2uib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rdma_cluster.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "rdma_link.h"

namespace c2uib
{

int c4uib_seed_process(RDMA_Cluster** cluster, int argc, char** argv)
{
    //FIXME: throw exception if *cluster is null or invalid

    //TODO: add utility to parse a file: C4UIB cluster application configuration
    if (argc < 5) // 1_cmnd + 2_min_nodes + 1_node_num + 1_app_argc
    {
        return (0);
    }

    // script or cmndline function will provide node_index and app_argc
    int app_argc = atoi(argv[argc-1]);  // original argc before HCA list.
    int node_index = atoi(argv[argc-2]);// this node's index in HCA list.
    int num_nodes = argc-app_argc-2;   // total number of nodes HCA list.

    if (app_argc<1 || node_index<0 || node_index>=num_nodes || num_nodes<2)
    {
        return (0);
    }

    // each node joins cluster using the list of nodes at argv[app_argc]
    (*cluster) = new RDMA_Cluster(node_index, num_nodes, argv+app_argc);

    if (!(*cluster) || !(*cluster)->isCluster())
    {
        return (0);
    }

    return (app_argc);
}


RDMA_Cluster::RDMA_Cluster(uint32_t hca_index, uint32_t count, char** hca_names)
    : node_index_(hca_index)
    , node_count_(count)
    , rdma_link_vector_(node_count_)
{
    connectNodes(rdma_link_vector_,hca_index,hca_names);
};


RDMA_Cluster::~RDMA_Cluster()
{
};


void RDMA_Cluster::connectNodes(std::vector<RDMA_Link>& links, uint32_t node, char** list, int port)
{
    //alternate server / client and increment port.
    int server_port_off=0, client_port_off=node/2;
    uint32_t oddN = node&1;

    //start servers first_, non blocking.
    uint32_t index = 0;
    for (auto iter = links.begin(); iter != links.end(); iter++)
    {
        iter->SetPortNum(port); //default starting port#
        bool xtype = oddN ^ (index & 1);   //true when only one end is odd
        
        if ((index < node && xtype)
                || (index > node && !xtype))
        {
            // FIXME: Is this really what we want to do? Looks like we are double incrementing.
            iter->SetPortNum(iter->GetPortNum() + server_port_off++);
            // l->port += server_port_off++;// Increment port# for each new server
            iter->Listen(0);    //,true);//0 = a service thread will continue waiting for client
        }
        ++index;
   }

    index = 0;
    //connect blocking clients to non-blocking servers
    for (auto iter = links.begin(); iter != links.end(); iter++)
    {
        bool xtype = oddN ^ (index & 1);   //true when only one end is odd

        if ((index < node && !xtype)
                || (index > node && xtype))
        {
            if (index < node && !oddN)
            {
                // l->port += (client_port_off - 1);
                iter->SetPortNum(iter->GetPortNum() + (client_port_off - 1));
            }
            else
            {
                // l->port += client_port_off;    // Increment port# for each new client
                iter->SetPortNum(iter->GetPortNum() + client_port_off);
            }

            if (!iter->Connect(list[index],10))      //,true))//max retry time in sec.
            {
                fprintf(stderr,"%d) client %s : %d failed\n", node, list[index], iter->GetPortNum());
            }
        }
        ++index;
    }

    index = 0;
    //time-out wait for servers to complete client connect
    for (auto iter = links.begin(); iter != links.end(); iter++)
    {
        bool xtype = oddN ^ (index & 1);   //true when only one end is odd

        if ((index < node && xtype)
                || (index > node && !xtype))
        {
            if (!iter->Listen(10))      //,true))//max wait time in sec.
            {
                fprintf(stderr,"%d) link %s : %d server not connected\n", node, list[index], iter->GetPortNum());
            }
        }
        ++index;
    }
}

void RDMA_Cluster::registerMemory(void* addr, size_t size, uint32_t share_id)
{
    // post recv of SHARED_MEM_INFO on all link channels
    if (share_id!= (uint32_t)-1)
    {
        uint32_t index = 0;
        for (auto iter = rdma_link_vector_.begin(); iter != rdma_link_vector_.end(); iter++)
        {
            if (index != node_index_)
            {
                iter->recv_shared_info();
            }
            ++index;
        }
    }

    uint32_t index = 0;
    // register and share this memory on all link channels
    for (auto iter = rdma_link_vector_.begin(); iter != rdma_link_vector_.end(); iter++)
    {
        if (index != node_index_)
        {
            iter->register_memory(addr, size, share_id!= (uint32_t)-1?true:false, share_id);
        }
        ++index;
    }

    if (share_id!= (uint32_t)-1)
    {
        // blocking wait till all async recv msg()s complete
        uint32_t index = 0;
        for (auto iter = rdma_link_vector_.begin(); iter != rdma_link_vector_.end(); iter++)
        {
            if (index != node_index_)
            {
                iter->recv_wait();
            }
            ++index;
        }
    }

    //FIXME: unregister l->shmem_remote.back(), registered in message_recv
};


void RDMA_Cluster::rdmaWrite(uint32_t local_reg, size_t local_offset, uint32_t remote_node, uint32_t remote_reg, size_t remote_offset, size_t size, bool block)
{
    if (block)
    {
        RDMA_Link::WORK_ID wr_id;
        rdma_link_vector_.at(remote_node).shmem_write(rdma_link_vector_.at(remote_node).GetMemoryRegion(local_reg), local_offset, & (rdma_link_vector_.at(remote_node).GetRemoteSharedMemoryInfo(remote_reg)), remote_offset, size, (uint64_t) &wr_id);
        wr_id.complete();
    }
    else
    {
        rdma_link_vector_.at(remote_node).shmem_write(rdma_link_vector_.at(remote_node).GetMemoryRegion(local_reg), local_offset, & (rdma_link_vector_.at(remote_node).GetRemoteSharedMemoryInfo(remote_reg)), remote_offset, size);
    }
}


//FIXME: Sync functions need exception to test link<node_count_
void RDMA_Cluster::initSync(uint32_t link)
{
    rdma_link_vector_.at(link).initSync();//message_recv(sync_msg,sizeof(sync_msg));
}

void RDMA_Cluster::procSync(uint32_t link)
{
    rdma_link_vector_.at(link).procSync();//recv_wait();
}

void RDMA_Cluster::initSync(uint32_t link, void* msg_in, size_t size)
{
    rdma_link_vector_.at(link).message_recv(msg_in, size);
}

void RDMA_Cluster::procSync(uint32_t link, void* msg_out, size_t size)
{
    rdma_link_vector_.at(link).message_send(msg_out, size, true);
    rdma_link_vector_.at(link).recv_wait();
}

} // namespace c2uib

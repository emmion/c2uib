
# Copyright 2016 Emmion.  All rights reserved.
# 
# This file is part of c2uib.
# 
# c2uib is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# c2uib is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with c2uib.  If not, see <http://www.gnu.org/licenses/>.

set(HEADERS
    rdma_cluster.h
    rdma_link.h
    rdma_point.h
)
set(SRCS
    rdma_link.cpp
    rdma_cluster.cpp
    rdma_point.cpp
)

include_directories(${RDMA_INCLUDE_DIR})

add_library(${PROJECT_NAME_STR} SHARED ${HEADERS} ${SRCS})
target_link_libraries(${PROJECT_NAME_STR} ${RDMA_LIBRARY})

install(FILES ${HEADERS}  DESTINATION include/${PROJECT_NAME_STR})
install(TARGETS ${PROJECT_NAME_STR} LIBRARY DESTINATION lib)

/*
 * Copyright 2010 - 2012 Creative Consultants LLC.  All rights reserved.
 *
 * Creative Consultants and its licenser retain all intellectual property
 * and proprietary rights in and to this software and related documentation.
 * Any use, reproduction, disclosure, or distribution of this software
 * and related documentation without an express license agreement from
 * Creative Consultants is strictly prohibited.
 *
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "rdma_point.h"

using namespace c2uib;

// $ g++ -Wall ~/c2uib/tests/simple_link.cpp -I../c2uib/ -L./ -lc2uib  -lpthread -lrdmacm -libverbs -o simple_link
// $ export LD_LIBRARY_PATH=./

//to execute on remote node use 2 terminals.
//terminal 1: $ ssh ccn001 "LD_LIBRARY_PATH=$(pwd) $(pwd)/simple_link -"
//terminal 2: $ ./simple_link ibn01
//to execute on same node: $ ./simple_link

int main(int argc, char* argv[])
{
    if (!strcmp(argv[1], "noop"))
    {
        exit(EXIT_SUCCESS);
    }

    int size=1000000000;
    char* buffer = (char*)malloc(size);
    RDMA_Point* link;

    // no arg = fork; else <hostname>; or '-',server
    pid_t pID = (argc>1?(argv[1][0]=='-'?0:1):fork());

    // nodeA: child &| server
    if (pID == 0)
    {
        memset(buffer,0,size);
        fputs("nodeA: connecting as server\n",stderr);
        link = new RDMA_Point();

        fputs("nodeA: register shared memory\n",stderr);
        link->regMem(buffer,size,-1);

        link->recvReg();
        fputs("nodeA: received memory registration\n",stderr);
        link->initSync();

        link->read(size,true);
        int i=0;

        while (i<size)
        {
            if (!buffer[i++])
            {
                break;
            }
        }
        
        if (i==size)
        {
            fputs("nodeA: RDMA read data valid\n",stderr);
        }
        else
        {
            fputs("nodeA: RDMA read failed\n",stderr);
        }

        link->procSync();
    }
    else // nodeB: parent &| client
    {
        memset(buffer,1,size);
        fputs("nodeB: connecting as client\n",stderr);
        link = new RDMA_Point(argc>1?argv[1]:(char*)"localhost");

        fputs("nodeB: register shared memory\n",stderr);
        link->regMem(buffer,size,0);

        fputs("nodeB: sent memory registration\n",stderr);
        link->initSync();

        fputs("nodeB: waiting for RDMA read to complete\n",stderr);
        link->procSync();
    }

    delete link;
    delete buffer;
    return (0);
}

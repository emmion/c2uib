/*
 * Copyright 2010 - 2012 Creative Consultants LLC.  All rights reserved.
 *
 * Creative Consultants and its licenser retain all intellectual property
 * and proprietary rights in and to this software and related documentation.
 * Any use, reproduction, disclosure, or distribution of this software
 * and related documentation without an express license agreement from
 * Creative Consultants is strictly prohibited.
 *
 */

// $ g++ -Wall ~/c2uib/tests/test_rdma_link.cpp -I../c2uib/ -L./ -lc2uib  -lpthread -lrdmacm -libverbs -o test_rdma_link
// $ export LD_LIBRARY_PATH=./

//  Execute: c4run ./test_rdma_link arg1 arg2 "ibn00 ibn01 ibn02 ibn03 ibn04"
//  The cluster of HCA nodes is seeded using the list in quotes. Use c4run.sh
//  or copy this function to the command line on any one of the cluster nodes.
//  Any arguments can be given after the application name and before HCA list.
/*____________________________________________________________________

function c4run() \
{  trap '' SIGTSTP; trap '' SIGINT; trap '' SIGQUIT;\
   HCA_INDEX=0;\
   for n in ${!#} ;\
      do ssh -x $n  cd $(pwd)';' LD_LIBRARY_PATH=$LD_LIBRARY_PATH $@ $HCA_INDEX $(( $#-1 ))&\
      let "HCA_INDEX=HCA_INDEX+1";\
   done;\
   echo "----------- Control signals are trapped. Any other key will kill & exit. ------------";\
   read -n 1 c;\
   trap SIGTSTP; trap SIGINT; trap SIGQUIT;\
   for n in ${!#} ;\
      do ssh -x $n pkill -9 -f $1 >/dev/null 2>&1;\
   done;\
};\
typeset -fx c4run
________________________________________________________________________*/


#include <unistd.h>
#include <string.h>

#include <cstdio>
#include <cstdlib>

#include "rdma_cluster.h"

using namespace c2uib;

int main(int argc, char** argv)
{
    if (!strcmp(argv[1], "noop"))
    {
        exit(EXIT_SUCCESS);
    }

    RDMA_Cluster* cluster;
    int app_argc = c4uib_seed_process(&cluster, argc, argv);

    if (!app_argc)
    {
        fputs("***  Failed: see test_rdma_link.cpp for execution instructions\n",stderr);
        exit(EXIT_FAILURE);
    }

    sleep(2);//they get 2 sec. to connect

    fprintf(stderr,"%d) Hello cluster\n",cluster->getHCAIndex());

    exit(EXIT_SUCCESS);
}


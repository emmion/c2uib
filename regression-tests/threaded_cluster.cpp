/*
 * Copyright 2010 - 2012 Creative Consultants LLC.  All rights reserved.
 *
 * Creative Consultants and its licenser retain all intellectual property
 * and proprietary rights in and to this software and related documentation.
 * Any use, reproduction, disclosure, or distribution of this software
 * and related documentation without an express license agreement from
 * Creative Consultants is strictly prohibited.
 *
 */

// $ g++ -Wall ~/c2uib/tests/threaded_cluster.cpp -I../c2uib/ -L./ -lc2uib  -lpthread -lrdmacm -libverbs -o threaded_cluster
// $ export LD_LIBRARY_PATH=./

//  Execute: c4run ./threaded_cluster arg1 arg2 "ibn00 ibn01 ibn02 ibn03 ibn04"
//  The cluster of HCA nodes is seeded using the list in quotes. Use c4run.sh
//  or copy this function to the command line on any one of the cluster nodes.
//  Any arguments can be given after the application name and before HCA list.
/*____________________________________________________________________

function c4run() \
{  trap '' SIGTSTP; trap '' SIGINT; trap '' SIGQUIT;\
   HCA_INDEX=0;\
   for n in ${!#} ;\
      do ssh -x $n cd $(pwd)';' LD_LIBRARY_PATH=$LD_LIBRARY_PATH $@ $HCA_INDEX $(( $#-1 ))&\
      let "HCA_INDEX=HCA_INDEX+1";\
   done;\
   read -n 1 c;\
   trap SIGTSTP; trap SIGINT; trap SIGQUIT;\
   for n in ${!#} ;\
      do ssh -x $n pkill -9 -f $1 >/dev/null 2>&1;\
   done;\
};\
typeset -fx c4run
________________________________________________________________________*/

#include <unistd.h>
#include <string.h>

#include <cstdio>
#include <cstdlib>

#include "rdma_cluster.h"

using namespace c2uib;

RDMA_Cluster* cluster;

int main(int argc, char** argv)
{
    if (!strcmp(argv[1], "noop"))
    {
        exit(EXIT_SUCCESS);
    }

    int app_argc = c4uib_seed_process(&cluster, argc, argv);

    if (app_argc < 2)
    {
        if (app_argc)
        {
            fputs("incorrect arguments: requires number on threads per node\n",stderr);
            delete(cluster);
        }

        exit(EXIT_FAILURE);
    }

    // Yee ! Hah ! Your cluster application is now seeded on it's HCAs.

    // next we initialize the cluster with thread-ables.
    int num_threads = atoi(argv[1]);

//TODO: Thread-ables below

    int process_id = cluster->getHCAIndex();
    int proc_count = cluster->getHCACount();

    // has a data set with PROC_SIZE available for every process.
    const size_t PROC_SIZE = 1000000;
    size_t num_bytes = PROC_SIZE * proc_count * sizeof(int);
    int* proc_data = (int*)malloc(num_bytes);

    // enter only this process's data into this process's data set
    size_t data_index = process_id * PROC_SIZE;

    while (data_index < (process_id+1) * PROC_SIZE)
    {
        proc_data[data_index++] = process_id;    //a value to be verified
    }

    // register this proc_data set as shared memory on the cluster.
    // we're only using one shared memory registration per process.
    const uint32_t REG_INDEX = 0; // for local and remote
    cluster->registerMemory(proc_data, num_bytes, process_id);

    // initialize a process completion sync for all processes
    for (int proc=0; proc<proc_count; proc++)
    {
        if (proc != process_id) // do not sync with self
        {
            cluster->initSync(proc);
        }
    }

    // all processes will use RDMA to write their data to all others
    data_index = process_id * PROC_SIZE * sizeof(int);

    for (int proc=0; proc<proc_count; proc++)
    {
        if (proc != process_id) // do not write to self
        {
            cluster->rdmaWrite(REG_INDEX, data_index, proc, REG_INDEX, data_index, PROC_SIZE*sizeof(int), true);
        }
    }

    // wait for all nodes to complete with a poor mans fence
    for (int proc=0; proc<proc_count; proc++)
    {
        if (proc != process_id) // do not write to self
        {
            cluster->procSync(proc);
        }
    }

    // verify the completed data set has every processes data
    for (int proc=0; proc<proc_count; proc++)
    {
        for (data_index = proc * PROC_SIZE;
                data_index < (proc+1) * PROC_SIZE;
                data_index++)
            if (proc_data[data_index] != proc)
            {
                exit(EXIT_FAILURE);
            }
    }

    fprintf(stderr,"%d) Hello cluster %d\n",process_id,num_threads);

//TODO: thread-ables join here

    exit(EXIT_SUCCESS);
}


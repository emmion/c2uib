#!/bin/bash

# usage: run-reg1.sh <host> <first-node-num> <last-node-num>
# usage: run-reg1.sh ibn 00 05

HOST_PREFIX=$1
START=$2
END=$3

function c4run() {
    trap '' SIGTSTP
    trap '' SIGINT
    trap '' SIGQUIT
    HCA_INDEX=0
    for n in ${!#}
    do
        ssh -x $n cd $(pwd)';' LD_LIBRARY_PATH=$LD_LIBRARY_PATH $@ $HCA_INDEX $(( $#-1 ))&      let "HCA_INDEX=HCA_INDEX+1";
    done
    sleep 5
    trap SIGTSTP
    trap SIGINT
    trap SIGQUIT
    for n in ${!#}
    do
        ssh -x $n pkill -9 -f $1 >/dev/null 2>&1
    done
}
typeset -fx c4run

TEMPF=`mktemp`

HOSTS=`eval echo ${HOST_PREFIX}{${START}..${END}}`
COUNT=`expr ${END} - ${START} + 1`

#echo ${HOSTS}
#echo ${COUNT}

LD_LIBRARY_PATH=src c4run regression-tests/test-rdma-link arg1 arg2 "${HOSTS}" > ${TEMPF} 2>&1

OCCUR=`grep 'Hello cluster' ${TEMPF} | wc -l`

if [ ${OCCUR} -eq ${COUNT} ]; then
    echo 'CORRECT' ${OCCUR} ${COUNT}
    EX=0
else
    echo 'INCORRECT' ${OCCUR} ${COUNT}
    EX=1
fi

rm ${TEMPF}

exit ${EX}

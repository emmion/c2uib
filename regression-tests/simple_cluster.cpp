/*
 * Copyright 2010 - 2012 Creative Consultants LLC.  All rights reserved.
 *
 * Creative Consultants and its licenser retain all intellectual property
 * and proprietary rights in and to this software and related documentation.
 * Any use, reproduction, disclosure, or distribution of this software
 * and related documentation without an express license agreement from
 * Creative Consultants is strictly prohibited.
 *
 */

// $ g++ -Wall ~/c2uib/tests/simple_cluster.cpp -I../c2uib/ -L./ -lc2uib  -lpthread -lrdmacm -libverbs -o simple_cluster
// $ export LD_LIBRARY_PATH=./

//  Execute: c4run ./simple_cluster arg1 arg2 "ibn00 ibn01 ibn02 ibn03 ibn04"
//  Or: LD_LIBRARY_PATH=./ c4run bin/simple_cluster arg1 arg2 "ibn00 ibn01 ibn02 ibn03 ibn04"
//  The cluster of HCA nodes is seeded using the list in quotes. Use c4run.sh
//  or copy this function to the command line on any one of the cluster nodes.
//  Any arguments can be given after the application name and before HCA list.
/*____________________________________________________________________

function c4run() \
{  trap '' SIGTSTP; trap '' SIGINT; trap '' SIGQUIT;\
   HCA_INDEX=0;\
   for n in ${!#} ;\
      do ssh -x $n cd $(pwd)';' LD_LIBRARY_PATH=$LD_LIBRARY_PATH $@ $HCA_INDEX $(( $#-1 ))&\
      let "HCA_INDEX=HCA_INDEX+1";\
   done;\
   read -n 1 c;\
   trap SIGTSTP; trap SIGINT; trap SIGQUIT;\
   for n in ${!#} ;\
      do ssh -x $n pkill -9 -f $1 >/dev/null 2>&1;\
   done;\
};\
typeset -fx c4run
________________________________________________________________________*/

#include <unistd.h>
#include <string.h>

#include <cstdio>
#include <cstdlib>

#include "rdma_cluster.h"

using namespace c2uib;

int main(int argc, char** argv)
{
    if (!strcmp(argv[1], "noop"))
    {
        exit(EXIT_SUCCESS);
    }

    if (argc < 5) // 1_cmnd + 2_min_nodes + 1_node_num + 1_app_argc
    {
        fputs("see source .cpp for examples of how to execute on multiple nodes.\n",stderr);
        exit(EXIT_FAILURE);
    }

    // script or cmndline function will provide node_index and app_argc
    int app_argc = atoi(argv[argc-1]);  // original argc before HCA list.
    int node_index = atoi(argv[argc-2]);// this node's index in HCA list.
    int num_nodes = argc-app_argc-2;   // total number of nodes HCA list.

    if (app_argc<1 || node_index<0 || node_index>=num_nodes || num_nodes<2)
    {
        exit(EXIT_FAILURE);
    }

    // each node joins cluster using the list of nodes at argv[app_argc]
    RDMA_Cluster cluster(node_index, num_nodes, argv+app_argc);

    if (!cluster.isCluster())
    {
        exit(EXIT_FAILURE);
    }

    // Yee ! Hah ! This cluster application is now seeded on it's HCAs.
    // next we would notify the cluster of our threadables, initially 1.
    // Threadables are not required in this example because......

    // The application will run 1 process per node. Each process
    // has a data set with PROC_SIZE available for every process.
    int process_id = node_index;
    int proc_count = num_nodes;
    const size_t PROC_SIZE = 1000000;
    size_t num_bytes = PROC_SIZE * proc_count * sizeof(int);
    int* proc_data = (int*)malloc(num_bytes);

    // enter only this process's data into this process's data set
    size_t data_index = process_id * PROC_SIZE;

    while (data_index < (process_id+1) * PROC_SIZE)
    {
        proc_data[data_index++] = process_id;    //a value to be verified
    }

    // register this proc_data set as shared memory on the cluster.
    // we're only using one shared memory registration per process.
    const uint32_t REG_INDEX = 0; // for local and remote
    cluster.registerMemory(proc_data, num_bytes, process_id);

    // initialize a process completion sync for all processes
    for (int proc=0; proc<proc_count; proc++)
    {
        if (proc != process_id) // do not sync with self
        {
            cluster.initSync(proc);
        }
    }
    
    // all processes will use RDMA to write their data to all others
    data_index = process_id * PROC_SIZE * sizeof(int);

    for (int proc=0; proc<proc_count; proc++)
    {
        if (proc != process_id) // do not write to self
        {
            cluster.rdmaWrite(REG_INDEX, data_index, proc, REG_INDEX, data_index, PROC_SIZE*sizeof(int), true);
        }
    }
    
    // wait for all nodes to complete with a poor mans fence
    for (int proc=0; proc<proc_count; proc++)
    {
      if (proc != process_id) // do not write to self
        {
            cluster.procSync(proc);
        }
    }
    
    // verify the completed data set has every processes data
    for (int proc=0; proc<proc_count; proc++)
    {
        for (data_index = proc * PROC_SIZE;
                data_index < (proc+1) * PROC_SIZE;
                data_index++)
        {
            if (proc_data[data_index] != proc)
            {
                exit(EXIT_FAILURE);
            }
        }
    }
    
    fprintf(stderr,"%d) Hello cluster\n",process_id);

    exit(EXIT_SUCCESS);
}

